#include "main.h"

int program::main_windows(int argc, char *argv[]) {
#ifdef  _WIN32
    int errorCode = 0;
    for (int i = 1; i < argc; i++) {
        // Program options
        if (strncmp(argv[i], "-h", 2) == 0) {
            this->help();
            return EXIT_SUCCESS;
        }
        if (strncmp(argv[i], "-v", 2) == 0) {
            this->version();
            return EXIT_SUCCESS;
        }
        if (strncmp(argv[i], "-d", 2) == 0 && i + 1 < argc) {
            errorCode = this->params.set_targetDirectory(argv[++i]);
        }
        if (strncmp(argv[i], "-s", 2) == 0 && i + 1 < argc) {
            errorCode = this->params.set_outputSelectors(argv[++i]);
        }
        if (strncmp(argv[i], "-c", 2) == 0 && i + 1 < argc) {
            errorCode = this->params.set_configFile(argv[++i]);
            if (errorCode == 0) {
                config conf;
                errorCode = conf.overrideParams(&this->params, this->params.get_configFile());
            }
        }

        // Algorithm options
        if (strncmp(argv[i], __OPT_TIMESTEP_NUM.c_str(), __OPT_TIMESTEP_NUM.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_timeStepNum(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_TIMESTEP_SKIP.c_str(), __OPT_TIMESTEP_SKIP.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_timeStepSkip(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_TURBINE_DRIVE.c_str(), __OPT_TURBINE_DRIVE.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_turbineDrive(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_TURBINE_OFFSET_X.c_str(), __OPT_TURBINE_OFFSET_X.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_turbineOffsetX(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_TURBINE_RADIUS.c_str(), __OPT_TURBINE_RADIUS.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_turbineRadius_m(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_WIND_SPEED.c_str(), __OPT_WIND_SPEED.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_windSpeed(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_WING_PROFILE.c_str(), __OPT_WING_PROFILE.length()) == 0 && i + 1 < argc) {
            this->params.set_wingProfile(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_WING_LENGTH.c_str(), __OPT_WING_LENGTH.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_wingLength(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_WING_NUM.c_str(), __OPT_WING_NUM.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_wingNum(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_WING_PITCH.c_str(), __OPT_WING_PITCH.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_wingPitch(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_X_SIZE.c_str(), __OPT_X_SIZE.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_xSize(argv[++i]);
        }
        if (strncmp(argv[i], __OPT_Y_SIZE.c_str(), __OPT_Y_SIZE.length()) == 0 && i + 1 < argc) {
            errorCode = this->params.set_ySize(argv[++i]);
        }
    }

    errorCode = this->checkMandatoryParameters();
    if (errorCode != 0) {
        return errorCode;
    }

#endif
    return this->lbm();
}

int program::main_linux(int argc, char *argv[]) {
#ifdef __linux__
    static struct option longOptions[] = {
        { OPT_TIMESTEP_NUM.c_str(), required_argument, 0, 0 },
        { OPT_TIMESTEP_SKIP.c_str(), required_argument, 0, 0 },
        { OPT_TURBINE_DRIVE.c_str(), required_argument, 0, 0 },
        { OPT_TURBINE_OFFSET_X.c_str(), required_argument, 0, 0 },
        { OPT_TURBINE_RADIUS.c_str(), required_argument, 0, 0 },
        { OPT_WIND_SPEED.c_str(), required_argument, 0, 0 },
        { OPT_WING_LENGTH.c_str(), required_argument, 0, 0 },
        { OPT_WING_NUM.c_str(), required_argument, 0, 0 },
        { OPT_WING_PITCH.c_str(), required_argument, 0, 0 },
        { OPT_WING_PROFILE.c_str(), required_argument, 0, 0 },
        { OPT_X_SIZE.c_str(), required_argument, 0, 0 },
        { OPT_Y_SIZE.c_str(), required_argument, 0, 0 },
        { 0, 0, 0, 0 }
    };

    int i = 0, c = 0, errorCode = 0;
    while ((c = getopt_long(argc, argv, "d:s:c:hv", longOptions, &i)) != -1) {
        switch (c) {
            // Program options
            case 'h':
                this->help();
                return EXIT_SUCCESS;
            case 'v':
                this->version();
                return EXIT_SUCCESS;
            case 'd':
                errorCode = this->params.set_targetDirectory(optarg);
                break;
            case 's':
                errorCode = this->params.set_outputSelectors(optarg);
                break;
            case 'c':
                errorCode = this->params.set_configFile(optarg);
                if (errorCode == 0) {
                    config conf;
                    errorCode = conf.overrideParams(&this->params, this->params.get_configFile());
                }
                break;

                // Algorithm options
            case 0:
                if(strncmp(longOptions[i].name, OPT_TIMESTEP_NUM.c_str(), OPT_TIMESTEP_NUM.length()) == 0) {
                    errorCode = this->params.set_timeStepNum(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_TIMESTEP_SKIP.c_str(), OPT_TIMESTEP_SKIP.length()) == 0) {
                    errorCode = this->params.set_timeStepSkip(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_TURBINE_DRIVE.c_str(), OPT_TURBINE_DRIVE.length()) == 0) {
                    errorCode = this->params.set_turbineDrive(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_TURBINE_OFFSET_X.c_str(), OPT_TURBINE_OFFSET_X.length()) == 0) {
                    errorCode = this->params.set_turbineOffsetX(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_TURBINE_RADIUS.c_str(), OPT_TURBINE_RADIUS.length()) == 0) {
                    errorCode = this->params.set_turbineRadius_m(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_WIND_SPEED.c_str(), OPT_WIND_SPEED.length()) == 0) {
                    errorCode = this->params.set_windSpeed(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_WING_LENGTH.c_str(), OPT_WING_LENGTH.length()) == 0) {
                    errorCode = this->params.set_wingLength(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_WING_NUM.c_str(), OPT_WING_NUM.length()) == 0) {
                    errorCode = this->params.set_wingNum(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_WING_PITCH.c_str(), OPT_WING_PITCH.length()) == 0) {
                    errorCode = this->params.set_wingPitch(optarg);
                }
                if (strncmp(longOptions[i].name, OPT_WING_PROFILE.c_str(), OPT_WING_PROFILE.length()) == 0) {
                    errorCode = this->params.set_wingProfile(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_X_SIZE.c_str(), OPT_X_SIZE.length()) == 0) {
                    errorCode = this->params.set_xSize(optarg);
                }
                if(strncmp(longOptions[i].name, OPT_Y_SIZE.c_str(), OPT_Y_SIZE.length()) == 0) {
                    errorCode = this->params.set_ySize(optarg);
                }
                break;
        }
    }

    errorCode = this->checkMandatoryParameters();
    if (errorCode != 0) {
        return errorCode;
    }
#endif
    return this->lbm();;
}

int main(int argc, char *argv[]) {
    program program;
#ifdef  _WIN32
    return program.main_windows(argc, argv);
#endif
#ifdef __linux__
    return program.main_linux(argc, argv);
#endif
}

int program::lbm() {
    // Parameter array
    const int parameterArraySize = PARAM_ARRAY_SIZE;
    float parameterArray[parameterArraySize] = {};
    this->initializeParameterArray(parameterArray);
    float *g_parameterArray;
    cudaMalloc(&g_parameterArray, parameterArraySize * sizeof(float));

    const int xSize = this->params.get_xSize();
    const int ySize = this->params.get_ySize();

    // ParticleDistributionFunction written to by the propagation step
    const int f_size = xSize * ySize * 9;
    float *f_in = new float[f_size];
    float *g_f_in;
    cudaMalloc(&g_f_in, f_size * sizeof(float));

    // ParticleDistributionFunction written to by the collision step
    float *f_out = new float[f_size];
    float *g_f_out;
    cudaMalloc(&g_f_out, f_size * sizeof(float));

    // Stores the result velocities in X and Y direction
    const int velocities_size = xSize * ySize * 2;
    float *velocities = new float[velocities_size];
    float *g_velocities;
    cudaMalloc(&g_velocities, velocities_size * sizeof(float));

    // Stores the result density
    const int density_size = xSize * ySize;
    float *densities = new float[density_size];
    float *g_densities;
    cudaMalloc(&g_densities, density_size * sizeof(float));

    // Stores the resulting torque
    const int torque_size = xSize * ySize;
    float *torques = new float[velocities_size];
    float *g_torques;
    cudaMalloc(&g_torques, torque_size * sizeof(float));

    // Directions array
    char *g_directions;
    char *g_reflectionDirections;
    cudaMalloc(&g_directions, 18 * sizeof(char));
    cudaMalloc(&g_reflectionDirections, 9 * sizeof(char));

    // Weights array
    float *g_weights;
    cudaMalloc(&g_weights, 9 * sizeof(float));

    // WingProfile
    std::vector<float> wingProfileVector = this->params.get_wingProfile();
    if (!checkWingProfile(wingProfileVector)) {
        this->print.error("Wing profile invalid.");
        return INVALID_WING_PROFILE;
    }
    const int wingProfileSize = wingProfileVector.size();
    float *g_wingprofile;
    float *wingProfile = wingProfileVector.data();
    cudaMalloc(&g_wingprofile, wingProfileSize * sizeof(float));

    this->printInfo();
    results out = results(this->params);

    // Initialize LBM
    init(f_in, DIRECTIONS, parameterArray);
    cudaMemcpy(g_parameterArray, parameterArray, parameterArraySize * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(g_f_in, f_in, f_size * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(g_f_out, f_out, f_size * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(g_directions, DIRECTIONS, 18 * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(g_reflectionDirections, REFLECTION_DIRECTIONS, 9 * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(g_weights, WEIGHTS, 9 * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(g_wingprofile, wingProfile, wingProfileSize * sizeof(float), cudaMemcpyHostToDevice);

    // Define blocks and grid
    const int blockWidth = 8;
    const int gridX = (xSize + (blockWidth - 1)) / blockWidth;
    const int gridY = (ySize + (blockWidth - 1)) / blockWidth;
    const dim3 blockDimension(blockWidth, blockWidth, 1);
    const dim3 gridDimension(gridX, gridY, 1);

    std::chrono::high_resolution_clock::time_point startTotal = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point startStep = std::chrono::high_resolution_clock::now();

    for (int timeStep = 0; timeStep < this->params.get_timeStepNum(); timeStep++) {
        collide << <gridDimension, blockDimension >> > (g_f_in, g_f_out, g_velocities, g_densities, g_torques, g_directions, g_reflectionDirections, g_weights, g_wingprofile, g_parameterArray);
        stream << <gridDimension, blockDimension >> > (g_f_in, g_f_out, g_directions, g_parameterArray);

        cudaError_t err = cudaGetLastError();
        if (err != cudaSuccess) {
            print.error(cudaGetErrorString(err));
        }

        if (timeStep % this->params.get_timeStepSkip() == 0) {
            if (this->params.get_doOutputVelocities()) {
                cudaMemcpy(velocities, g_velocities, velocities_size * sizeof(float), cudaMemcpyDeviceToHost);
                out.velocities(velocities);
            }

            if (this->params.get_doOutputDensities()) {
                cudaMemcpy(densities, g_densities, density_size * sizeof(float), cudaMemcpyDeviceToHost);
                out.densities(densities);
            }

            if (this->params.get_doOutputTorque()) {
                cudaMemcpy(torques, g_torques, torque_size * sizeof(float), cudaMemcpyDeviceToHost);
                out.torques(torques);
            }

            std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
            long long duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - startStep).count();
            print.info("Timestep " + std::to_string(timeStep) + " took " + std::to_string(duration) + " ms");
            startStep = std::chrono::high_resolution_clock::now();
        }
    }

    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    long long duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - startTotal).count();
    print.info("Total execution took " + std::to_string(duration) + "ms.");
    print.info("Done, checkout " + this->params.get_targetDirectory());

    cudaFree(g_parameterArray);
    cudaFree(g_f_in);
    cudaFree(g_f_out);
    cudaFree(g_velocities);
    cudaFree(g_densities);
    cudaFree(g_directions);
    cudaFree(g_torques);
    cudaFree(g_reflectionDirections);
    cudaFree(g_weights);

    return EXIT_SUCCESS;
}

void program::initializeParameterArray(float *params) {
    this->params.unitOfDistance = this->params.get_turbineRadius_m() / this->params.get_turbineRadius(); // dx
    this->params.unitOfTime = this->params.unitOfDistance / CS_REFERENCE; // dt
    this->params.unitOfMass = DENSITY_AIR * (pow(this->params.unitOfDistance, 3)); // dm
    this->params.unitOfForce = this->params.unitOfMass / (this->params.unitOfTime * this->params.unitOfTime);
    this->params.unitOfTorque = this->params.unitOfForce * this->params.unitOfDistance;

    const int turbineRadius = this->params.get_turbineRadius(); // nOf cells
    const float convertedSpeed = this->params.get_windSpeed() * this->params.unitOfTime / this->params.unitOfDistance; // m/s -> unit free
    const float convertedViscosity = VISCOSITY_AIR * this->params.unitOfDistance * this->params.unitOfTime / this->params.unitOfMass;
    const float convertedOmega = 1 / (3.f * convertedViscosity + 0.5f); // Relaxationtime to fit the given viscosity
    const float convertedRevolutionFrequency = this->params.get_turbineDrive() * this->params.unitOfTime; // 1/s -> unit free
    const float convertedWingLength = this->params.get_wingLength() / this->params.unitOfDistance; // m -> unit free

    // Parameters that don't need unit-conversion
    params[PARAM_X_SIZE] = this->params.get_xSize();
    params[PARAM_Y_SIZE] = this->params.get_ySize();
    params[PARAM_WING_NUM] = this->params.get_wingNum();
    params[PARAM_WING_PITCH] = this->params.get_wingPitch();
    params[PARAM_TURBINE_RADIUS] = turbineRadius;
    params[PARAM_TURBINE_CIRCUMFERENCE] = 2 * PI * turbineRadius;
    params[PARAM_TURBINE_OFFSET_X] = this->params.get_turbineOffsetX();

    // Parameters that need unit-conversion
    params[PARAM_WIND_SPEED] = convertedSpeed;
    params[PARAM_WING_LENGTH] = convertedWingLength;    
    params[PARAM_TURBINE_DRIVE] = convertedRevolutionFrequency;
    params[PARAM_OMEGA_FORCES_CORRECTED] = 1 / (1 + convertedOmega);
    params[PARAM_OMEGA_DISTRIBUTIONS_CORRECTED] = convertedOmega / (1 + convertedOmega);
}

int program::checkMandatoryParameters() {
    if(strlen(this->params.get_targetDirectory().c_str()) <= 0) {
        print.error("-d: this option is mandatory");
        return OPTION_NOT_SET;
    }

    return EXIT_SUCCESS;
}

bool program::checkWingProfile(std::vector<float> wingProfile) {
    return wingProfile.size() > 0;
}

void program::printInfo() {
    print.info("Running LBM with the following settings:");
    if (!this->params.get_configFile().empty()) {
        print.info("(config file applied: " + this->params.get_configFile() + ")");
    }
    print.info("  Timesteps: " + std::to_string(this->params.get_timeStepNum()) + ", print every " + std::to_string(this->params.get_timeStepSkip()) + ". timestep");
    print.info("  Resolution: " + std::to_string(this->params.get_xSize()) + " x " + std::to_string(this->params.get_ySize()) + " cells");
    print.info("  Turbine rotation frequency: " + std::to_string(this->params.get_turbineDrive()) + " 1/s");
    print.info("  Turbine offset from left: " + std::to_string(this->params.get_turbineOffsetX()));
    print.info("  Turbine radius: " + std::to_string(this->params.get_turbineRadius_m()) + "m");
    print.info("  Wind speed: " + std::to_string(this->params.get_windSpeed()) + " m/s");
    print.info("  Wing profile: " + this->params.get_wingProfileName());
    print.info("  Wing pitch: " + std::to_string(this->params.get_wingPitch()));
    print.info("  Wing count: " + std::to_string(this->params.get_wingNum()));
    print.info("  Wing length: " + std::to_string(this->params.get_wingLength()) + "m");
}

void program::help() {
    wing_profiles wingProfiles;
    std::vector<string> wingProfileNames = wingProfiles.getWingProfileNames();
    std::stringstream joinedWingProfileNames;
    string delimiter = "  ";
    copy(wingProfileNames.begin(), wingProfileNames.end(), std::ostream_iterator<string>(joinedWingProfileNames, delimiter.c_str()));

    printf(
            "usage: \033[36m%s\033[0m -d <this is where the output files go> -[program options] --[algorithm options]\n"
            "\n"
            "program options:\n"
            "-c        provide an algorithm config file\n"
            "            format: (you can use all algorithm parameters listed below without the prefix dashes)\n"
            "            %% comment\n"
            "            wind-speed: 23\n"
            "            turbine-radius: 3\n"
            "            wing-length: 96\n"
            "-d*       output directory\n"
            "-h        display this help\n"
            "-s        select ouput with acronym(s)\n"
            "            default: output everything\n"
            "            selectors: v (veocities), d (densities), t (torque)\n"
            "            example: -s vt (to omit densities outpt)\n"
            "-v        display version info\n"
            "\n"
            "algorithm options:\n"
            "--count               num of timesteps to process in total\n"
            "--skip                how many steps to skip before printing a file\n"
            "--turbine-radius      radius of the turbine in [m]\n"
            "--turbine-offset-x    offset of the turbine from the left inlet in [cells]\n"
            "--turbine-drive       speed of the turbine in [1/s]\n"
            "--wind-speed          speed of wind in [m/s]\n"
            "--wing-length         length of a single wing in [m]\n"
            "--wing-num            wing count\n"
            "--wing-pitch          pitch of the wing in [..]\n"
            "--wing-profile        wing profile as file or name\n"
            "                      Available wing profile names:\n"
            "                        %s\n"
            "                      The file must be in CSV and in the following format:\n"
            "                        alpha;CL;CD\n"
            "                        alpha;CL;CD\n"
            "                        alpha;...\n"
            "--x-size              resolution in X direction\n"
            "--y-size              resolution in Y direction\n"
            "\n"
            "arguments marked with a star * are mandatory\n",
        BINARY_NAME.c_str(), joinedWingProfileNames.str().c_str());
}

void program::version() {
    string os;
#ifdef _WIN32
    os = "Windows";
#endif
#ifdef __linux__
    os = "Linux";
#endif
    printf(
            "\033[36m%s\033[0m (%s) version %s\n"
            "compiled on " __DATE__ " at " __TIME__ "\n"
            "GitLab repository: https://gitlab.com/alcedo-atthis/source\n",
            PROGRAM_NAME.c_str(), os.c_str(), PROGRAM_VERSION.c_str());
}
