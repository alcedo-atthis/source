#include "program_params.h"

int program_params::set_targetDirectory(const char *targetDirectory) {
	struct stat info;
	if ((stat(targetDirectory, &info) != 0) || !(info.st_mode & S_IFDIR)) {
		string targetDirectoryS = targetDirectory;
		print.error("-d: '" + targetDirectoryS + "' does not exist or is a file");
		return FILE_NOT_FOUND;
	}
	this->targetDirectory = targetDirectory;
	return EXIT_SUCCESS;
}

string program_params::get_targetDirectory() {
	return this->targetDirectory;
}

int program_params::set_configFile(const char *configFile) {
	struct stat info;
	if ((stat(configFile, &info) != 0) || (info.st_mode & S_IFDIR)) {
		string configFileS = configFile;
		print.warn("-c: '" + configFileS + "' not found");
		return FILE_NOT_FOUND;
	}
	this->configFile = configFile;
	return EXIT_SUCCESS;
}

string program_params::get_configFile() {
	return this->configFile;
}

int program_params::set_outputSelectors(const char *selectors) {
	string velocitiesSelector = "v";
	string densitiesSelector = "d";
	string torqueSelector = "t";
	string selected = string(selectors);

	if (selected.find(velocitiesSelector) == std::string::npos) {
		this->doOutputVelocities = false;
	}
	if (selected.find(densitiesSelector) == std::string::npos) {
		this->doOutputDensities = false;
	}
	if (selected.find(torqueSelector) == std::string::npos) {
		this->doOutputTorque = false;
	}

	return 0;
}

bool program_params::get_doOutputVelocities() {
	return this->doOutputVelocities;
}

bool program_params::get_doOutputDensities() {
	return this->doOutputDensities;
}

bool program_params::get_doOutputTorque() {
	return this->doOutputTorque;
}

int program_params::set_wingProfile(const char *wingProfile) {
    this->wingProfileName = wingProfile;
    struct stat info;
    bool useFile = (stat(wingProfile, &info) == 0) && !(info.st_mode & S_IFDIR);
    if (useFile) {
        parseWingProfile(wingProfile);
    }
    else {
        this->wingProfile = this->wingProfiles.getWingProfile(wingProfile);
    }
	this->wingProfileInitialized = true;

    return EXIT_SUCCESS;
}

vector<float> program_params::get_wingProfile() {
    if (!this->wingProfileInitialized) {
        print.warn("--wing-profile: No wing profile given. Fallback to '" + this->wingProfileName + "' wing profile.");
        this->set_wingProfile(this->wingProfileName.c_str());
    }

    return this->wingProfile;
}

string program_params::get_wingProfileName() {
    return this->wingProfileName;
}

void program_params::parseWingProfile(string wingProfileFile) {
    std::ifstream data(wingProfileFile);
    string line;
    while (std::getline(data, line)) {
        std::stringstream lineStream(line);
        string cell;
        while (std::getline(lineStream, cell, ';')) {
            this->wingProfile.push_back(std::stof(cell));
        }
    }
}

int program_params::set_xSize(const char *xSize) {
    try {
        this->xSize = std::stoi(xSize);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_X_SIZE + ": invalid value");
        return INVALID_INTEGER;
    }
}

int program_params::get_xSize() {
    return this->xSize;
}

int program_params::set_ySize(const char *ySize) {
    try {
        this->ySize = std::stoi(ySize);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_Y_SIZE + ": invalid value");
        return INVALID_INTEGER;
    }
}

int program_params::get_ySize() {
    return this->ySize;
}

int program_params::set_windSpeed(const char *windSpeed) {
    try {
        this->windSpeed = std::stof(windSpeed);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_WIND_SPEED + ": invalid value");
        return INVALID_FLOAT;
    }
}

float program_params::get_windSpeed() {
    return this->windSpeed;
}

int program_params::set_wingPitch(const char *wingPitch) {
    try {
        this->wingPitch = std::stof(wingPitch);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_WING_PITCH + ": invalid value");
        return INVALID_FLOAT;
    }
}

float program_params::get_wingPitch() {
    return this->wingPitch;
}

int program_params::set_wingNum(const char *wingNum) {
    try {
        this->wingNum = std::stoi(wingNum);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_WING_NUM + ": invalid value");
        return INVALID_INTEGER;
    }
}

int program_params::get_wingNum() {
    return this->wingNum;
}

int program_params::set_wingLength(const char *wingLength) {
    try {
        this->wingLength = std::stof(wingLength);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_WING_LENGTH + ": invalid value");
        return INVALID_FLOAT;
    }
}

float program_params::get_wingLength() {
    return this->wingLength;
}

int program_params::set_turbineDrive(const char *turbineDrive) {
    try {
        this->turbineDrive = std::stof(turbineDrive);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_TURBINE_DRIVE + ": invalid value");
        return INVALID_FLOAT;
    }
}

float program_params::get_turbineDrive() {
    return this->turbineDrive;
}

int program_params::set_turbineOffsetX(const char *turbineOffsetX) {
    try {
        this->turbineOffsetX = std::stoi(turbineOffsetX);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_TURBINE_OFFSET_X + ": invalid value");
        return INVALID_INTEGER;
    }
}

int program_params::get_turbineOffsetX() {
	if (this->turbineOffsetX == 0) {
		return this->xSize / 3;
	}
    return this->turbineOffsetX;
}

int program_params::set_turbineRadius_m(const char *turbineRadius) {
    try {
        this->turbineRadius_m = std::stof(turbineRadius);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_TURBINE_RADIUS + ": invalid value");
        return INVALID_INTEGER;
    }
}

float program_params::get_turbineRadius_m() {
	return this->turbineRadius_m;
}

int program_params::get_turbineRadius() {
	return (int) this->get_ySize() * TURBINE_RADIUS_SCALE;
}

int program_params::set_timeStepNum(const char *timeStepNum) {
    try {
        this->timeStepNum = std::stoi(timeStepNum);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_TIMESTEP_NUM + ": invalid value");
        return INVALID_INTEGER;
    }
}

int program_params::get_timeStepNum() {
    return this->timeStepNum;
}

int program_params::set_timeStepSkip(const char *timeStepSkip) {
    try {
        this->timeStepSkip = std::stoi(timeStepSkip);
        return EXIT_SUCCESS;
    }
    catch (std::exception) {
        print.error(OPT_TIMESTEP_SKIP + ": invalid value");
        return INVALID_INTEGER;
    }
}

int program_params::get_timeStepSkip() {
    return this->timeStepSkip;
}
