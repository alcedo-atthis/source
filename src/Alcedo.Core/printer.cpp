#include "printer.h"

void printer::info(string message) {
	std::cout << ". " << message << std::endl;
}

void printer::warn(string message) {
	std::cout << "\033[1;33m" << ": (warning) " << message << "\033[0m" << std::endl;
}

void printer::error(string message) {
	std::cout << "\033[1;31m" << "! (error) " << message << "\033[0m" << std::endl;
}
