#define _CRT_SECURE_NO_DEPRECATE

#ifndef RESULTS_H
#define RESULTS_H

#include <string>
#include <time.h>
#include <fstream>
#include <math.h>
#include <algorithm>

#include "lbm.h"
#include "program_params.h"
#include "printer.h"
#include "constants.h"
#include "config.h"

#ifdef _WIN32
#include <Windows.h>
#include <tchar.h>
#endif

using std::string;

class results
{
public:
	results(program_params params);
	void velocities(float *data);
	void densities(float *data);
	void torques(float *data);

private:
	const string fileExtension = ".dat";
	const string velocitiesTitle = "velocities";
	const string densitiesTitle = "densities";
	const string torqueTitle = "torque";
	program_params params;
	printer print;
	int velocitiesResultsCounter = 1;
	int densitiesResultsCounter = 1;
	string targetDirectoryWithDate;
	std::tm *createDate;

	string getPathDelimiter();
	bool initializeDirectory();
	bool createDirectory(string fullPath);
	bool deleteDirectory(string fullPath);
	void createTargetDirectoryWithDateName();
	void createInfoFile();
	void getDirectedVelocities(float *velocities, float *directedVelocities);
	float getTotalTorque(float *torques);
	void addDimensionPrefix(float *array, int arrayLength, float *resultArray);
	void append1dArray(string name, float data);
	void append2dArray(string groupName, int index, float *data);
};

#endif // !RESULTS_H
