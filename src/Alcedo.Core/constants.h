#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

using std::string;

// Program specific
const string PROGRAM_NAME = "alcedo";
const string PROGRAM_VERSION = "1.0";
const string BINARY_NAME = "alcedo";

// Error codes
const int FILE_NOT_FOUND = 101;
const int INVALID_WING_PROFILE = 102;
const int INVALID_INTEGER = 201;
const int INVALID_FLOAT = 202;
const int OPTION_NOT_SET = 301;

// LBM
// Lattice directions
enum Direction {
	STATIONARY = 0,
	RIGHT = 1,
	UP = 2,
	LEFT = 3,
	DOWN = 4,
	UP_RIGHT = 5,
	UP_LEFT = 6,
	DOWN_RIGHT = 7,
	DOWN_LEFT = 8
};

// Lattice velocities
constexpr char DIRECTIONS[18] = {
	 0,  0,	// STATIONARY
	 1,  0,	// RIGHT
	 0, -1, // UP
	-1,  0, // LEFT
	 0,  1, // DOWN
	 1, -1, // UP_RIGHT
	-1, -1, // UP_LEFT
	 1,  1, // DOWN_RIGHT
	-1,  1	// DOWN_LEFT
};

constexpr char REFLECTION_DIRECTIONS[9] = { 0, 3, 4, 1, 2, 8, 7, 6, 5 }; // Reversed indices from DIRECTIONS array e.g. left = 3 -> right = 1

// Weights for Equilibrium distribution
constexpr float WEIGHTS[9] = {
	4.f / 9.f,
	1.f / 9.f,
	1.f / 9.f,
	1.f / 9.f,
	1.f / 9.f,
	1.f / 36.f,
	1.f / 36.f,
	1.f / 36.f,
	1.f / 36.f
};

// Math
constexpr float PI = 3.1415926f;
constexpr float TURBINE_RADIUS_SCALE = 1.f / 16.f;

constexpr float CS_AIR = 343.f; // Speed of sound in air [m/s]
constexpr float CS_2 = 1.f / 3.f; // Speed of sound squared [unitfree] (speed of sound = 1/sqrt(3))
constexpr float CS_REFERENCE = 588.8972746f; // Speed of sound for lattice-speed of 1 [m/s]

constexpr float DENSITY_AIR = 1.185f; // [kg/m^3]
constexpr float VISCOSITY_AIR = 0.00001831f; // Viscosity in air at 20 degrees celsius [kg/m s]

// Parameters array
const int PARAM_ARRAY_SIZE = 12; // ! Update this when params are added or removed
const int PARAM_X_SIZE = 0;
const int PARAM_Y_SIZE = 1;
const int PARAM_WIND_SPEED = 2;
const int PARAM_TURBINE_RADIUS = 3;
const int PARAM_TURBINE_OFFSET_X = 4;
const int PARAM_TURBINE_DRIVE = 5;
const int PARAM_TURBINE_CIRCUMFERENCE = 6;
const int PARAM_OMEGA_DISTRIBUTIONS_CORRECTED = 7;
const int PARAM_OMEGA_FORCES_CORRECTED = 8;
const int PARAM_WING_NUM = 9;
const int PARAM_WING_LENGTH = 10;
const int PARAM_WING_PITCH = 11;

// Option identifiers
const string OPT_TIMESTEP_NUM = "count";
const string OPT_TIMESTEP_SKIP = "skip";
const string OPT_TURBINE_DRIVE = "turbine-drive";
const string OPT_TURBINE_OFFSET_X = "turbine-offset-x";
const string OPT_TURBINE_RADIUS = "turbine-radius";
const string OPT_WIND_SPEED = "wind-speed";
const string OPT_WING_LENGTH = "wing-length";
const string OPT_WING_NUM = "wing-num";
const string OPT_WING_PITCH = "wing-pitch";
const string OPT_WING_PROFILE = "wing-profile";
const string OPT_X_SIZE = "x-size";
const string OPT_Y_SIZE = "y-size";

const string __OPT_TIMESTEP_NUM = "--" + OPT_TIMESTEP_NUM;
const string __OPT_TIMESTEP_SKIP = "--" + OPT_TIMESTEP_SKIP;
const string __OPT_TURBINE_DRIVE = "--" + OPT_TURBINE_DRIVE;
const string __OPT_TURBINE_OFFSET_X = "--" + OPT_TURBINE_OFFSET_X;
const string __OPT_TURBINE_RADIUS = "--" + OPT_TURBINE_RADIUS;
const string __OPT_WIND_SPEED = "--" + OPT_WIND_SPEED;
const string __OPT_WING_LENGTH = "--" + OPT_WING_LENGTH;
const string __OPT_WING_NUM = "--" + OPT_WING_NUM;
const string __OPT_WING_PITCH = "--" + OPT_WING_PITCH;
const string __OPT_WING_PROFILE = "--" + OPT_WING_PROFILE;
const string __OPT_X_SIZE = "--" + OPT_X_SIZE;
const string __OPT_Y_SIZE = "--" + OPT_Y_SIZE;

#endif // !CONSTANTS_H
