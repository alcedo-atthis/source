#ifndef LBM_H
#define LBM_H

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "constants.h"

// Index helper makros
#define INDEX_3D(x, y, z) (x + xSize * (y + ySize * z))
#define INDEX_2D() (x + xSize * y)
#define DIRECTION_X(i) (2 * i)
#define DIRECTION_Y(i) (2 * i + 1)

// Forward declarations
void init(float *f_in, const char *directions, const float *params);
__global__ void collide(float *f_in, float *f_out, float *velocities, float *densities, float *torques, const char *directions, const char *reflectionDirections, const float *weights, const float *wing_profile, const float *params);
__global__ void stream(float *f_in, float *f_out, const char *directions, const float *params);

#endif // !LBM_H
