#ifndef PRINTER_H
#define PRINTER_H

#include <string>
#include <iostream>

using std::string;

class printer
{
public:
	void info(string message);
	void warn(string message);
	void error(string message);
};

#endif // #PRINTER_H
