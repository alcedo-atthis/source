#include "lbm.h"

__device__ float localDensity(float *f_in, const int x, const int y, const float *params) {
	const int xSize = (int)params[PARAM_X_SIZE];
	const int ySize = (int)params[PARAM_Y_SIZE];

	float density = 0.0;
	for (int direction = 0; direction < 9; direction++) {
		density += f_in[INDEX_3D(x, y, direction)];
	}
	return density;
}

__device__ void localVelocity(float *f_in, const int x, const int y, const char *directions, const float density, float *output, const float *params) {
	const int xSize = (int)params[PARAM_X_SIZE];
	const int ySize = (int)params[PARAM_Y_SIZE];
	
	for (int direction = 0; direction < 9; direction++) {
		output[0] += f_in[INDEX_3D(x, y, direction)] * directions[DIRECTION_X(direction)];
		output[1] += f_in[INDEX_3D(x, y, direction)] * directions[DIRECTION_Y(direction)];
	}
	output[0] /= density;
	output[1] /= density;
}

__device__ bool isCellOnCircle(const int x, const int y, const float *params) {
	const int ySize = (int)params[PARAM_Y_SIZE];
	const int turbineRadius = (int)params[PARAM_TURBINE_RADIUS];
	const double turbineOffsetX = (double)params[PARAM_TURBINE_OFFSET_X];

	double distanceX = turbineOffsetX - x;
	double distanceY = (double)ySize / 2 - y;
	return ((int)sqrt(distanceX * distanceX + distanceY * distanceY)) == turbineRadius;
}

__device__ void find_nearest_neighborIndices(const float value, const float *wingProfile, const int nOfRows, int *output) {
	for (int i = 0; i < nOfRows - 1; i++) {
		float xi = wingProfile[i * 3];
		float xi1 = wingProfile[(i + 1) * 3];

		if (xi <= value && xi1 >= value) {
			output[0] = i;
			output[1] = i + 1;
			return;
		}
	}
}

__device__ float linear_interpolation(const float x0, const float y0, const float x1, const float y1, const float x) {
	if (x <= x0) { return y0; }
	if (x >= x1) { return y1; }

	float t = x - x0;
	t = t / (x1 - x0);
	return y0 + t * (y1 - y0);
}

__device__ float calculatePhi(const int x, const int y, const float *params) {
	const int ySize = (int)params[PARAM_Y_SIZE];
	const int turbineOffsetX = (int)params[PARAM_TURBINE_OFFSET_X];

	const int centerX = turbineOffsetX;
	const int centerY = ySize / 2;
	const double distanceX = abs(centerX - x);
	const double distanceY = abs(centerY - y);

	if (x <= centerX && y <= centerY) { // first quadrant of the circle
		return atan(distanceX / distanceY);
	}
	else if (x <= centerX && y > centerY) { // second quadrant of the circle
		return atan(distanceY / distanceX) + PI / 2;
	}
	else if (x > centerX && y > centerY) { // third quadrant of the circle
		return atan(distanceX / distanceY) + PI;
	}
	else { // fourth quadrant of the circle
		return atan(distanceY / distanceX) + 3 * PI / 2;
	}
}

__device__ void reflect(const int x, const int y, float *f_in, float *f_out, const char *reflectionDirections, const float *params) {
	const int xSize = (int)params[PARAM_X_SIZE];
	const int ySize = (int)params[PARAM_Y_SIZE];

	for (int direction = 0; direction < 9; direction++) {
		int reflectedDirection = reflectionDirections[direction];
		f_out[INDEX_3D(x, y, reflectedDirection)] = f_in[INDEX_3D(x, y, direction)];
	}
}

__device__ void calculateWingForces(const int x, const int y, const float density, float *velocity, const float *wingProfile, float *output, const float *params) {
	const int turbineRadius = (int)params[PARAM_TURBINE_RADIUS];
	const int turbineCircumference = (int)params[PARAM_TURBINE_CIRCUMFERENCE];
	const float turbineDrive = params[PARAM_TURBINE_DRIVE];
	const float pitch = params[PARAM_WING_PITCH];
	const float wingLength = params[PARAM_WING_LENGTH];
	const float wingNum = params[PARAM_WING_NUM];

	const float RHO = 1.185;
	const float phi = calculatePhi(x, y, params);

	float cw = cos(phi);
	float sw = sin(phi);

	// Einheitsvektoren radial und tangential
	float eR[2] = { -sw, cw };
	float eT[2] = { cw, sw };

	float omegaPi = -2.f * PI * turbineRadius * turbineDrive;
	float vT[2] = { omegaPi * eT[0], omegaPi * eT[1] };

	float w[2] = { velocity[0] - vT[0], velocity[1] - vT[1] };
	float w2 = w[0] * w[0] + w[1] * w[1];
	float w_length = sqrt(w2);

	float eD[2] = { w[0] / w_length, w[1] / w_length };
	float eL[2] = { eD[1], -eD[0] };

	float temp = eD[0] * eR[0] + eD[1] * eR[1];
	float eDeTDotProduct = eD[0] * eT[0] + eD[1] * eT[1];

	// Fulfill boundary conditions for acos
	if (eDeTDotProduct > 1) { eDeTDotProduct = 1; }
	else if (eDeTDotProduct < -1) { eDeTDotProduct = -1; }

	// Attack angle
	float alpha = 0.f;
	if (temp > 0.f) {
		alpha = -acos(eDeTDotProduct) - pitch;
	}
	else {
		alpha = acos(eDeTDotProduct) - pitch;
	}

	// Lift and drag
	float d = 180 * alpha / PI;
	const int nOfRows = 154;
	// Make sure d is within our known datarange
	if (d < wingProfile[0]) {
		d = wingProfile[0];
	}
	else if (d > wingProfile[(nOfRows - 1) * 3]) {
		d = wingProfile[(nOfRows - 1) * 3];
	}
	
	int neighborIndices[2] = {-1, -1};
	find_nearest_neighborIndices(d, wingProfile, nOfRows, neighborIndices);
	int alpha0_index = neighborIndices[0] * 3;
	int alpha1_index = neighborIndices[1] * 3;
	// Interpolate wing-profile to estimate lift&drag coefficients
	float cL = linear_interpolation(wingProfile[alpha0_index], wingProfile[alpha0_index + 1], wingProfile[alpha1_index], wingProfile[alpha1_index + 1], d);
	float cD = linear_interpolation(wingProfile[alpha0_index], wingProfile[alpha0_index + 2], wingProfile[alpha1_index], wingProfile[alpha1_index + 2], d);
	float c0 = 0.5f * RHO * w2 * wingLength * wingNum;

	float liftForceX = cL * c0 * eL[0];
	float liftForceY = cL * c0 * eL[1];
	float dragForceX = cD * c0 * eD[0];
	float dragForceY = cD * c0 * eD[1];

	output[0] = -((liftForceX + dragForceX) / turbineCircumference);
	output[1] = (liftForceY + dragForceY) / turbineCircumference; // In our system y increases downwards (opposed to upwards in usual mathematics)
}

__device__ void createInletDistributions(const int x, const int y, const int xSize, const int ySize, float *velocity, const float windSpeed, float * f_in, const float density) {
	//float yPhys = y - 1.5f;
	//int L = ySize - 2;
	//velocity[0] = (4 * windSpeed / (L*L)) * (yPhys * L - yPhys * yPhys); // Inlet with Poiseuille profile
	velocity[0] = windSpeed; // Inlet with uniform profile
	velocity[1] = 0.f;

	f_in[INDEX_3D(x, y, RIGHT)] = f_in[INDEX_3D(x, y, LEFT)] + (2.f / 3.f) * density * velocity[0];

	float correctionX = (1.f / 6.f) * density * velocity[0];

	f_in[INDEX_3D(x, y, UP_RIGHT)] = f_in[INDEX_3D(x, y, DOWN_LEFT)] +
		0.5f * (f_in[INDEX_3D(x, y, DOWN)] - f_in[INDEX_3D(x, y, UP)]) + correctionX;

	f_in[INDEX_3D(x, y, DOWN_LEFT)] = f_in[INDEX_3D(x, y, UP_RIGHT)] +
		0.5f * (f_in[INDEX_3D(x, y, UP)] + f_in[INDEX_3D(x, y, DOWN)]) + correctionX;
}

__device__ void createOutletDistributions(const int x, const int y, const int xSize, const int ySize, float *velocity, float * f_in) {
	float velocityX = -1.f + (f_in[INDEX_3D(x, y, STATIONARY)] + f_in[INDEX_3D(x, y, UP)] + f_in[INDEX_3D(x, y, DOWN)])
		+ 2 * (f_in[INDEX_3D(x, y, UP_RIGHT)] + f_in[INDEX_3D(x, y, DOWN_RIGHT)] + f_in[INDEX_3D(x, y, RIGHT)]);

	velocity[0] = velocityX;
	velocity[1] = 0.f;

	f_in[INDEX_3D(x, y, LEFT)] = f_in[INDEX_3D(x, y, RIGHT)] - 2.f / 3.f * velocityX;

	f_in[INDEX_3D(x, y, DOWN_LEFT)] = f_in[INDEX_3D(x, y, UP_RIGHT)] +
		0.5f * (f_in[INDEX_3D(x, y, UP)] - f_in[INDEX_3D(x, y, DOWN)]) - 1.f / 6.f * velocityX;

	f_in[INDEX_3D(x, y, UP_LEFT)] = f_in[INDEX_3D(x, y, DOWN_RIGHT)] +
		0.5f * (f_in[INDEX_3D(x, y, DOWN)] - f_in[INDEX_3D(x, y, UP)]) - 1.f / 6.f * velocityX;
}

__global__ void collide(float *f_in, float *f_out, float *velocities, float *densities, float *torques, const char *directions, const char *reflectionDirections, const float *weights, const float *wingProfile, const float *params) {
	const int x = blockIdx.x * blockDim.x + threadIdx.x;
	const int y = blockIdx.y * blockDim.y + threadIdx.y;

	const int xSize = (int)params[PARAM_X_SIZE];
	const int ySize = (int)params[PARAM_Y_SIZE];
	const float omegaDistributionsCorrected = (float)params[PARAM_OMEGA_DISTRIBUTIONS_CORRECTED];
	const float omegaForcesCorrected = (float)params[PARAM_OMEGA_FORCES_CORRECTED];
	const float windSpeed = (float)params[PARAM_WIND_SPEED];
	const float turbineOffsetX = (float)params[PARAM_TURBINE_OFFSET_X];

	if (x >= xSize || y >= ySize) return; // Boundary checks

	bool isWall = y == 0 || y == (ySize - 1);
	//bool isOnSquare = (y >= ySize / 4) && (y <= 3 * ySize / 4) && (x >= xSize / 4) && (x <= 3 * xSize / 4);
	if (isWall/* || isOnSquare*/) { // Reflect off of top & bottom wall
		reflect(x, y, f_in, f_out, reflectionDirections, params);
		return;
	}

	float density = localDensity(f_in, x, y, params);
	densities[INDEX_2D()] = density;

	float velocity[2] = {};
	if (x == 0 && y > 0 && y < ySize - 2) {
		createInletDistributions(x, y, xSize, ySize, velocity, windSpeed, f_in, density);
	}
	else if (x == xSize - 1 && y > 0 && y < ySize - 2) {
		createOutletDistributions(x, y, xSize, ySize, velocity, f_in);
	}
	else {
		localVelocity(f_in, x, y, directions, density, velocity, params);
	}

	velocities[INDEX_3D(x, y, 0)] = velocity[0];
	velocities[INDEX_3D(x, y, 1)] = velocity[1];
	float velocitySquared = velocity[0] * velocity[0] + velocity[1] * velocity[1];
	
	// Calculate wing-force
	float wingForces[2] = { 0, 0 };
	float torque = 0.f;
	if (isCellOnCircle(x, y, params)) {
		calculateWingForces(x, y, density, velocity, wingProfile, wingForces, params);
		const int relativeXPosition = x - turbineOffsetX;
		const int relativeYPosition = y - ySize / 2;
		torque = -1.f * relativeXPosition * wingForces[0] - relativeYPosition * wingForces[1]; // Negative because we need the force that's applied to the wing
	}
	torques[INDEX_2D()] = torque;

	for (int direction = 0; direction < 9; direction++) {
		float distribution = f_in[INDEX_3D(x, y, direction)];

		// Equilibrium
		float directionVelocityInnerProduct = velocity[0] * directions[DIRECTION_X(direction)] + velocity[1] * directions[DIRECTION_Y(direction)];
		float equilibrium = weights[direction] * density * (1.f +
			3.f * directionVelocityInnerProduct +
			0.5f * pow(3.f * directionVelocityInnerProduct, 2) -
			1.5f * velocitySquared);
		float f_out_i = distribution + omegaDistributionsCorrected * (equilibrium - distribution);

		// Apply force
		float cg = wingForces[0] * directions[DIRECTION_X(direction)] + wingForces[1] * directions[DIRECTION_Y(direction)];
		float volumetric_force = weights[direction] * density *
			(cg / CS_2 +
			(cg * directionVelocityInnerProduct) / pow(CS_2, 2) -
				(wingForces[0] * velocity[0] + wingForces[1] * velocity[1]) / CS_2);
		f_out_i += omegaForcesCorrected * volumetric_force;

		f_out[INDEX_3D(x, y, direction)] = f_out_i;
	}
}

__global__ void stream(float *f_in, float *f_out, const char *directions, const float *params) {
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

	const int xSize = (int)params[PARAM_X_SIZE];
	const int ySize = (int)params[PARAM_Y_SIZE];

	if (x >= xSize || y >= ySize) return; // Check boundary

	for (int direction = 0; direction < 9; direction++) {
		int neighborX = x + directions[DIRECTION_X(direction)];
		int neighborY = y + directions[DIRECTION_Y(direction)];

		if (neighborX < 0 || neighborX >= xSize || neighborY < 0 || neighborY >= ySize) continue;

		f_in[INDEX_3D(neighborX, neighborY, direction)] = f_out[INDEX_3D(x, y, direction)]; // Stream to neighbor cell
	}
}

void init(float *f_in, const char *directions, const float *params) {
	const int xSize = (int)params[PARAM_X_SIZE];
	const int ySize = (int)params[PARAM_Y_SIZE];
	//const float windSpeed = (float)params[PARAM_WIND_SPEED];

	// Initialize distributions
	//int L = ySize - 2;
	for (int x = 0; x < xSize; x++) {
		for (int y = 0; y < ySize; y++) {
			//float yPhys = y - 1.5f;
			//float ux = (4 * windSpeed / (L*L)) * (yPhys * L - yPhys * yPhys);

			for (int direction = 0; direction < 9; direction++) {
				//float cu = ux * directions[DIRECTION_X(direction)];
				//float poiseuille = (1 + cu + 0.5f * cu * cu - (3.f / 2.f * ux * ux));
				f_in[INDEX_3D(x, y, direction)] = WEIGHTS[direction];// * poiseuille;
			}
		}
	}
}
