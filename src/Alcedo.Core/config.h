#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>
#include <ctime>
#include <algorithm>

#include "constants.h"
#include "program_params.h"

using std::string;
using std::vector;

const string CONFIG_FILENAME = PROGRAM_NAME + ".cfg";

class config
{
    public:
        int overrideParams(program_params *params, string pathToConfig);
        string createInfoFileFromParams(program_params params, std::tm *dateTime);

    private:
        const string commentDelimiter = "%";
        const char keySuffix = ':';

        string cleanFromComment(string line);
};

#endif //!CONFIG_H
