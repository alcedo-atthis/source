#include "results.h"

results::results(program_params params) {
    this->params = params;
    this->initializeDirectory();
}

void results::velocities(float *data) {
    const int xSize = this->params.get_xSize();
    const int ySize = this->params.get_ySize();

	float *directedVelocities = new float[xSize * ySize];
	getDirectedVelocities(data, directedVelocities);

	this->append2dArray(this->velocitiesTitle, this->velocitiesResultsCounter, directedVelocities);
    this->velocitiesResultsCounter++;
}

void results::densities(float *data) {
    this->append2dArray(this->densitiesTitle, this->densitiesResultsCounter, data);
    this->densitiesResultsCounter++;
}

void results::torques(float *data) {
    float torque = getTotalTorque(data);
    this->append1dArray(this->torqueTitle, torque);
}

string results::getPathDelimiter() {
    string path_delimiter;
#ifdef _WIN32
    path_delimiter = "\\";
#endif
#ifdef __linux__
    path_delimiter = "/";
#endif
    return path_delimiter;
}

bool results::initializeDirectory() {
    this->createTargetDirectoryWithDateName();
    this->deleteDirectory(this->targetDirectoryWithDate);
    this->createDirectory(this->targetDirectoryWithDate);

    if (this->params.get_doOutputVelocities()) {
        this->createDirectory(this->targetDirectoryWithDate + this->getPathDelimiter() + this->velocitiesTitle);
    }
    if (this->params.get_doOutputDensities()) {
        this->createDirectory(this->targetDirectoryWithDate + this->getPathDelimiter() + this->densitiesTitle);
    }

    this->createInfoFile();
	return true;
}

void results::createTargetDirectoryWithDateName() {
    std::time_t now = std::time(NULL);
    this->createDate = localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "_%Y-%m-%d.%H-%M-%S", this->createDate);
    this->targetDirectoryWithDate = this->params.get_targetDirectory() + this->getPathDelimiter() + PROGRAM_NAME + string(buffer);
}

bool results::createDirectory(string fullPath) {
#ifdef _WIN32
    return CreateDirectory(fullPath.c_str(), NULL);
#endif
#ifdef __linux__
    return mkdir(fullPath.c_str(), 0755);
#endif
}

bool results::deleteDirectory(string fullPath) {
#ifdef _WIN32
    LPCTSTR directory = fullPath.c_str();
    SHFILEOPSTRUCT file_op = { NULL, FO_DELETE, directory, "", FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT, false, 0, "" };
    return SHFileOperation(&file_op) == 0;
#endif
#ifdef __linux__
    return system(("rm -rf " + fullPath).c_str());
#endif
}

void results::createInfoFile() {
    config conf;
    string data = conf.createInfoFileFromParams(this->params, this->createDate);

    std::ofstream file;
    file.open(this->targetDirectoryWithDate + this->getPathDelimiter() + CONFIG_FILENAME);
    file << data;
    file.close();
}

void results::getDirectedVelocities(float *velocities, float *directedVelocities) {
    const int xSize = this->params.get_xSize();
    const int ySize = this->params.get_ySize();

    int i = 0;
    for (int y = 0; y < ySize; y++) {
        for (int x = 0; x < xSize; x++) {
            float velocityX = velocities[INDEX_3D(x, y, 0)];
            float velocityY = velocities[INDEX_3D(x, y, 1)];
            float speed = sqrt(velocityX * velocityX + velocityY * velocityY);

            if (y == 0 || y == (ySize - 1)) { // Upper and lower wall
                speed = 0.f;
            }

            directedVelocities[i] = speed * this->params.unitOfDistance / this->params.unitOfTime; // Convert it back into m/s
            i++;
        }
    }
}

float results::getTotalTorque(float *torques) {
    const int xSize = this->params.get_xSize();
    const int ySize = this->params.get_ySize();

    float sum = 0.f;
    for (int y = 0; y < ySize; y++) {
        for (int x = 0; x < xSize; x++) {
            sum += torques[INDEX_2D()];
        }
    }
    return sum * this->params.unitOfTorque; // Convert it back from lattice-units to Nm
}

void results::addDimensionPrefix(float *array, int arrayLength, float *resultArray) {
	int resultArrayLength = arrayLength + 2;
	resultArray[0] = this->params.get_xSize();
	resultArray[1] = this->params.get_ySize();
	for (int i = 2; i < resultArrayLength; i++) {
		resultArray[i] = array[i - 2];
	}
}

void results::append1dArray(string name, float data) {
    FILE *file;
    string fileName = this->targetDirectoryWithDate + this->getPathDelimiter() + name + this->fileExtension;
    file = fopen(fileName.c_str(), "ab");
    fwrite(&data, sizeof(float), 1, file);
    fclose(file);
}

void results::append2dArray(string groupName, int index, float *data) {
    const int xSize = this->params.get_xSize();
    const int ySize = this->params.get_ySize();

	int dimensionPrefixedDataLength = (xSize * ySize) + 2;
	float *dimensionPrefixedData = new float[dimensionPrefixedDataLength];
	this->addDimensionPrefix(data, xSize * ySize, dimensionPrefixedData);

    FILE *file;
    string fileName = this->targetDirectoryWithDate + this->getPathDelimiter() + groupName + this->getPathDelimiter() + groupName + "_" + std::to_string(index) + this->fileExtension;
    file = fopen(fileName.c_str(), "wb");
    fwrite(dimensionPrefixedData, sizeof(float), dimensionPrefixedDataLength, file);
    fclose(file);
}
