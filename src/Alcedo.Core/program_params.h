#ifndef PROGRAM_PARAMS_H
#define PROGRAM_PARAMS_H

#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <fstream>
#include <sys/stat.h>

#include "printer.h"
#include "wing_profiles.h"
#include "constants.h"

using std::string;
using std::vector;

class program_params
{
    public:
		int set_targetDirectory(const char *targetDirectory);
		string get_targetDirectory();

		int set_configFile(const char *configFile);
		string get_configFile();

		int set_outputSelectors(const char *selectors);
		bool get_doOutputVelocities();
		bool get_doOutputDensities();
		bool get_doOutputTorque();

        int set_wingProfile(const char* wingProfile);
        vector<float> get_wingProfile();
        string get_wingProfileName();

        int set_xSize(const char *xSize);
        int get_xSize();

        int set_ySize(const char *ySize);
        int get_ySize();

        int set_windSpeed(const char *windSpeed);
        float get_windSpeed();

        int set_wingPitch(const char *wingPitch);
        float get_wingPitch();

        int set_wingNum(const char *wingPitch);
        int get_wingNum();

        int set_wingLength(const char *wingLength);
        float get_wingLength();

        int set_turbineDrive(const char *turbineDrive);
        float get_turbineDrive();

        int set_turbineOffsetX(const char *turbineOffsetX);
        int get_turbineOffsetX();

		int set_turbineRadius_m(const char *turbineRadius);
		float get_turbineRadius_m();

		int get_turbineRadius();

        int set_timeStepNum(const char *timeStepNum);
        int get_timeStepNum();

        int set_timeStepSkip(const char *timeStepSkip);
        int get_timeStepSkip();

		float unitOfDistance;
		float unitOfTime;
		float unitOfMass;
		float unitOfForce;
		float unitOfTorque;

    private:
        printer print;
        wing_profiles wingProfiles;

        string targetDirectory;
		string configFile;
        vector<float> wingProfile;
        string wingProfileName = "default";
        bool wingProfileInitialized = false;
        int xSize = 2000;
        int ySize = 1008;
        float windSpeed = 15.f; // [m/s]
        float wingPitch = 0.f;
        int wingNum = 3;
        float wingLength = 0.2f; // [m]
        float turbineDrive = 4.f; // [1/s]
        int turbineOffsetX = 0;
		int turbineRadius; // always 1/16
		float turbineRadius_m = 0.35f; // [m]
        int timeStepNum = 10000;
        int timeStepSkip = 100;
		bool doOutputVelocities = true;
		bool doOutputDensities = true;
		bool doOutputTorque = true;

        void parseWingProfile(string wingProfileFile);
};

#endif // !PROGRAM_PARAMS_H
