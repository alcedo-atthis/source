#ifndef MAIN_H
#define MAIN_H

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <chrono>
#include <math.h>

#include "constants.h"
#include "printer.h"
#include "program_params.h"
#include "wing_profiles.h"
#include "lbm.h"
#include "results.h"
#include "config.h"

#ifdef __linux__
#include <unistd.h>
#include <getopt.h>
#endif

using std::string;

class program
{
public:
	int main_windows(int argc, char *argv[]);
	int main_linux(int argc, char *argv[]);
	int lbm();

private:
	program_params params;
	printer print;

	void initializeParameterArray(float *params);
	bool checkWingProfile(std::vector<float> wingProfile);
    int checkMandatoryParameters();
	void printInfo();
	void help();
	void version();
};

#endif // !MAIN_H
