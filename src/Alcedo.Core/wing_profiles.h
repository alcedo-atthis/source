#ifndef PROFILES_H
#define PROFILES_H

#include <string>
#include <vector>
#include <map>

using std::string;
using std::vector;
using std::map;

class wing_profiles
{
public:
	wing_profiles();
	vector<float> getWingProfile(string name);
	vector<string> getWingProfileNames();

private:
	map<string, vector<float>> wingProfiles;
};

#endif // !PROFILES_H
