#include "config.h"

int config::overrideParams(program_params *params, string pathToConfig) {
	int errorCode = 0;

	std::ifstream data(pathToConfig);
	string line;
	while (std::getline(data, line)) {
		line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
		line = cleanFromComment(line);
		std::stringstream lineStream(line);
		string key;
		while (std::getline(lineStream, key, this->keySuffix)) {
			string value;
			std::getline(lineStream, value);

			if (key == OPT_TIMESTEP_NUM) errorCode = params->set_timeStepNum(value.c_str());
			if (key == OPT_TIMESTEP_SKIP) errorCode = params->set_timeStepSkip(value.c_str());
			if (key == OPT_TURBINE_DRIVE) errorCode = params->set_turbineDrive(value.c_str());
			if (key == OPT_TURBINE_OFFSET_X) errorCode = params->set_turbineOffsetX(value.c_str());
			if (key == OPT_TURBINE_RADIUS) errorCode = params->set_turbineRadius_m(value.c_str());
			if (key == OPT_WIND_SPEED) errorCode = params->set_windSpeed(value.c_str());
			if (key == OPT_WING_LENGTH) errorCode = params->set_wingLength(value.c_str());
			if (key == OPT_WING_NUM) errorCode = params->set_wingNum(value.c_str());
			if (key == OPT_WING_PITCH) errorCode = params->set_wingPitch(value.c_str());
			if (key == OPT_WING_PROFILE) errorCode = params->set_wingProfile(value.c_str());
			if (key == OPT_X_SIZE) errorCode = params->set_xSize(value.c_str());
			if (key == OPT_Y_SIZE) errorCode = params->set_ySize(value.c_str());
		}
	}

	return errorCode;
}

string config::cleanFromComment(string line) {
	return line.substr(0, line.find(this->commentDelimiter.c_str()));
}

string config::createInfoFileFromParams(program_params params, std::tm *dateTime) {
	string config;

	// Comment
	string os;
#ifdef _WIN32
	os = "Windows";
#endif
#ifdef __linux__
	os = "Linux";
#endif
	char dateBuffer[32];
	std::strftime(dateBuffer, 32, "%Y-%m-%d at %H:%M:%S", dateTime);
	config.append(this->commentDelimiter + "\n");
	config.append(this->commentDelimiter + " " + PROGRAM_NAME + " Version " + PROGRAM_VERSION + " on " + os + "\n");
	config.append(this->commentDelimiter + "\n");
	config.append(this->commentDelimiter + " Date: " + string(dateBuffer) + "\n");
	config.append(this->commentDelimiter + " Rerun with same configuration: alcedo -c alcedo.cfg -d <target-directory>\n");
	config.append(this->commentDelimiter + "\n");

	// Timesteps
	config.append("\n");
	config.append(this->commentDelimiter + " Timesteps:\n");
	config.append(OPT_TIMESTEP_NUM + this->keySuffix + " " + std::to_string(params.get_timeStepNum()) + "\n");
	config.append(OPT_TIMESTEP_SKIP + this->keySuffix + " " + std::to_string(params.get_timeStepSkip()) + "\n");

	// Turbine
	config.append("\n");
	config.append(this->commentDelimiter + " Turbine:\n");
	config.append(OPT_TURBINE_DRIVE + this->keySuffix + " " + std::to_string(params.get_turbineDrive()) + "\n");
	config.append(OPT_TURBINE_OFFSET_X + this->keySuffix + " " + std::to_string(params.get_turbineOffsetX()) + "\n");
	config.append(OPT_TURBINE_RADIUS + this->keySuffix + " " + std::to_string(params.get_turbineRadius()) + "\n");

	// Wind
	config.append("\n");
	config.append(this->commentDelimiter + " Wind:\n");
	config.append(OPT_WIND_SPEED + this->keySuffix + " " + std::to_string(params.get_windSpeed()) + "\n");

	// Wing
	config.append("\n");
	config.append(this->commentDelimiter + " Wing:\n");
	config.append(OPT_WING_LENGTH + this->keySuffix + " " + std::to_string(params.get_wingLength()) + "\n");
	config.append(OPT_WING_NUM + this->keySuffix + " " + std::to_string(params.get_wingNum()) + "\n");
	config.append(OPT_WING_PITCH + this->keySuffix + " " + std::to_string(params.get_wingPitch()) + "\n");
	config.append(OPT_WING_PROFILE + this->keySuffix + " " + params.get_wingProfileName() + "\n");

	// Resolution
	config.append("\n");
	config.append(this->commentDelimiter + " Resolution:\n");
	config.append(OPT_X_SIZE + this->keySuffix + " " + std::to_string(params.get_xSize()) + "\n");
	config.append(OPT_Y_SIZE + this->keySuffix + " " + std::to_string(params.get_ySize()) + "\n");

	return config;
}
