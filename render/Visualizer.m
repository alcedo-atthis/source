%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                                      %%%
%%%       alcedo Matlab Visualizer       %%%
%%%       Version 1.0                    %%%
%%%                                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear

% Select which alcedo input folder you want to render
input = "C:/Users/...";
% !

% Do not change the lines below
% !

% Variables
selectors2d = ["densities", "velocities"];
selectors1d = ["torque"];
% !

% Create output folder
outputPath = char(sprintf("%s/_render", input));
if ~exist(input, "dir")
    fprintf("! (error) The directory %s was not found\n", input);
    return;
elseif ~exist(outputPath, "dir")
    mkdir(outputPath);
end
% !

% Plot 2D array
for selector = selectors2d
    % Check folders and read files
    selectorInputPath = char(sprintf("%s/%s", input, selector));
    selectorOutputPath = char(sprintf("%s/%s", outputPath, selector));
    if ~exist(selectorInputPath, "dir")
        continue;
    elseif ~exist(selectorOutputPath, "dir")
        mkdir(selectorOutputPath);
    end
    files = dir(selectorInputPath);
    files = files(3:end);
    % !
    
    % Exclude already printed output
    remainingFiles = zeros();
    for fileIndex=1:length(files)
        expectedFileName = sprintf(selectorOutputPath + "/%s_%d.png", selector, fileIndex);
        if ~exist(expectedFileName, 'file')
            remainingFiles(fileIndex) = fileIndex;
        end
    end
    remainingFiles = nonzeros(remainingFiles);
    % !
    
    % Plot files
    parfor fileIndex = 1:length(remainingFiles)
        file = remainingFiles(fileIndex);
        inputFileName = sprintf("%s/%s_%i.dat", selectorInputPath, selector, file);
        outputFileName = sprintf("%s/%s_%d", selectorOutputPath, selector, file);

        fileId = fopen(inputFileName, 'r');
        dimensions = fread(fileId, 2, "float");
        xSize = dimensions(1);
        ySize = dimensions(2);
        data = fread(fileId, (xSize * ySize), "float");
        fclose(fileId);

        reshapedData = reshape(data, xSize, ySize);
        imagesc(reshapedData');

        bar = colorbar;
        bar.Label.String = selector;
        axis equal off;
        title(sprintf("%s over time", selector));

        print(char(outputFileName), '-dpng');
    end
    % ! Plot files
end
% ! Plot 2D array

% Plot 1D array
for selector = selectors1d
    % Check folders and read files
    selectorInputPath = sprintf("%s/%s.dat", input, selector);
    selectorOutputPath = sprintf("%s/%s.png", outputPath, selector);
    if ~exist(selectorInputPath, "file")
        continue;
    end
    % !
    
    % Plot file
    fileId = fopen(selectorInputPath, 'r');
    data = fread(fileId, inf, "float");
    fclose(fileId);
    plot(data);
    xlabel("Timestep");
    ylabel(selector);
    title(sprintf("%s over time", selector));
    print(char(selectorOutputPath), '-dpng');
    % ! Plot file
end
% ! Plot 1D array

fprintf(". Script exited, visit %s.\n", outputPath);
