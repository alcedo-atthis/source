# Alcedo

This is a tool to run the well-known Lattice-Boltzmann-Method (LBM), a computational fluid dynamics method. You can simulate fluids in combination with a vertical axial wind turbine. Several parameters are available to adjust the program to your needs. The software runs parallelized on a GPU by NVIDIA by using the CUDA framework. Therefore, you must have a CUDA enabled GPU in your machine.

1. [Features](#features)

2. [Usage](#usage)

    2.1 [Wing profiles](#wing-profiles)

    2.2 [Configuration files](#configuration-files)

    2.3 [Selectors](#selectors)

3. [Installation](#installation)

    3.1 [Linux](#linux)

    3.2 [Windows](#windows)

4. [Legal](#legal)

## Features
- runs parallelized on the GPU for fast rendering
- data plotable with Matlab for *velocities*, *densities* and overall *torque*
- configure the algorithm and the turbine with SI units
- algorithm configuration is storred in a config file and can be reused
- runs on Windows and Linux

## Usage
The usage is on both platforms the same, on Linux and Windows. Please make use of the `alcedo -h` command, as it explains you in detail how to use the software. This readme only gives a short introduction.

There are two parameter types the program understand, general program parameters, marked with a single `-` dash. An example is the `-d` parameter, which is mandatory and tells the program where the resulting files should be put. And there are double dashed `--` parameters, to control the algorithm, like `--turbine-radius` and so on.

The resulting files can be examined with the provided [Matlab](https://ch.mathworks.com/products/matlab.html "go to the Matlab website") scripts (`render/FluidVisualizer.m` and `render/PlotVisualizer.m`). To use these scripts, modify some parameters first (they are marked as that) and run the script.

### Wing profiles
Wing profiles are available in the Internet, for example via [Airfoil Tools](http://airfoiltools.com/ "Visit Airfoil Tools"). The program expects name of a built in profile (list them with the `-h` command) or a CSV file in this format:
```
alpha;CL;CD
alpha;CL;CD
alpha;...
```

Use the built in example wing profile:
```bash
$ alcedo -d ~/lbm --wing-profile example
```

Use a custom wing profile:
```bash
$ alcedo -d ~/lbm --wing-profile ~/.config/alcedo/myturbine.csv
```

Of course, this parameter can also be provided with a config file.

### Configuration files
With the `-c` program parameter you can specify a config file which contains algorithm parameters, like the above mentioned `--turbine-radius`. Any algorithm parameter additionaly given by running the program overrides the configuration parameters. The config file does not have to be complete, the usual fallbacks are given.

To run the algorithm with a predefined configuration and override the configurations x-size parameter, run:
```bash
$ alcedo -d ~/lbm -c ~/.configs/alcedo/myturbine.cfg --x-size 2000
```

The configuration file is also put to the program output so you can reuse it again and again.

All algorithm parameters are valid configuration file parameters. Just use them without the preceding double dash:

```matlab
% Percent characters start a line comment

% Timesteps:
count: 10000
skip: 100

% Turbine:
turbine-drive: 4
turbine-offset-x: 200
turbine-radius: 90

% Wind:
wind-speed: 11

% Wing:
wing-length: 0.23
wing-num: 3
wing-pitch: 0.96
wing-profile: default

% Resolution:
x-size: 1920
y-size: 1080
```

### Selectors
Use the `-s` program parameter to select your output. Consult the `alcedo -h` page for available selector. The `-s` parameter expects an acronym for the output to be selected, for example a `v` for velocities, a `d` for densities and so on. Multiple outputs can be selected. If no selector is set, everyting is put out. Selectors can help save storage as the output files may grow large.

If you're only interested in the velocities output:
```bash
$ alcedo -d ~/lbm -s v
```

If you're interested in the overall torque and timestep velocities:
```bash
$ alcedo -d ~/lbm -s vt
```

## Installation
### Linux
Before you proceed, make sure you have installed `make`, the proprietary NVIDIA kernel module, the CUDA runtime and driver and the CUDA Toolkit 10.0. Most of the software needed can be found on the [CUDA website](https://developer.nvidia.com/cuda-downloads "CUDA Website"). Then, compile the product this way:

1. After cloning the repository, change into the `src` folder and run `make`.
2. The executable is located in `src/bin/alcedo`. You can use it from there or place it where you like.

### Windows
Make sure, [Visual Studio 2017](https://visualstudio.microsoft.com/vs/ "Download Visual Studio 2017") and the [CUDA Toolkit 10.0](https://developer.nvidia.com/cuda-downloads "Download CUDA Toolkit 10.0") are installed.

1. Open the `src/Alcedo.sln` file with Visual Studio
2. Select the `Release` build configuration
3. Build the project
4. The executable is placed in `src/x64/Release/alcedo.exe`. You can use it from there or place it where you like.

## Legal
This project is the result of a seminar paper at the [Hochschule für Technik Rapperswil, Switzerland](https://www.hsr.ch/ "Visit www.hsr.ch"). This work was coached by Prof. Dr. Henrik Nordborg and Alain Schubiger. The software was developed by Mario Tarreghetta and Sebastian Hug.
